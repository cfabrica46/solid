package menu

import (
	"fmt"

	"github.com/cfabrica46/solid/solid-srp/store"
)

type Menu struct {
}

func NewMenu() Menu {
	return Menu{}
}

func (m Menu) View(products []store.Product) {
	var str string
	str += "\nID\tName\tPrice\n"
	str += "---------------------\n"

	for i := range products {
		str += fmt.Sprintf("%d\t%s\t%d\n", products[i].ID, products[i].Name, products[i].Price)
	}

	fmt.Print(str)
}
