package store

type Product struct {
	ID    int
	Name  string
	Price int
}

type Store struct {
	name     string
	products []Product
}

func NewStore(n string) Store {
	return Store{name: n}
}

func (s *Store) AddProducts(p []Product) {
	s.products = append(s.products, p...)
}

func (s Store) GetProducts() []Product {
	return s.products
}

func (s *Store) RemoveProductByID(id int) {
	for i := range s.products {
		if s.products[i].ID == id {
			s.products = append(s.products[:i], s.products[i+1:]...)
			return
		}
	}
}
