package main

import (
	"fmt"
	"log"

	"github.com/cfabrica46/solid/solid-ocp/menu"
	"github.com/cfabrica46/solid/solid-ocp/store"
)

func main() {
	products := []store.Product{
		{
			ID:    1,
			Name:  "tv",
			Price: 1000,
		},
		{
			ID:    2,
			Name:  "phone",
			Price: 100,
		},
		{
			ID:    3,
			Name:  "tablet",
			Price: 200,
		},
	}

	store := store.NewStore("my store")

	store.AddProducts(products)

	m := menu.NewMenu()
	/*
		result, err := m.View(store.GetProducts(), menu.NewMenuText())
		if err != nil {
			log.Fatal(err)
		} */

	result, err := m.View(store.GetProducts(), menu.NewMenuJSON())
	if err != nil {
		log.Fatal(err)
	}

	fmt.Println(result)
}
