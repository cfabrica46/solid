package menu

import (
	"encoding/json"
	"fmt"

	"github.com/cfabrica46/solid/solid-ocp/store"
)

type Menu struct {
}

func NewMenu() Menu {
	return Menu{}
}

func (m Menu) View(products []store.Product, mode MenuOutput) (result string, err error) {
	return mode.Output(products)
}

type MenuOutput interface {
	Output(products []store.Product) (result string, err error)
}

type MenuJSON struct {
}

type MenuText struct {
}

func NewMenuJSON() MenuJSON {
	return MenuJSON{}
}

func NewMenuText() MenuText {
	return MenuText{}
}

func (m MenuText) Output(products []store.Product) (result string, err error) {
	result += "\nID\tName\tPrice\n"
	result += "---------------------\n"

	for i := range products {
		result += fmt.Sprintf("%d\t%s\t%d\n", products[i].ID, products[i].Name, products[i].Price)
	}
	return
}

func (m MenuJSON) Output(products []store.Product) (result string, err error) {
	var resultByte []byte

	resultByte, err = json.MarshalIndent(products, "", " ")
	if err != nil {
		return
	}

	result = string(resultByte)
	return
}
