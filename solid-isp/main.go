package main

import "fmt"

type Pond struct {
}

func newPond() Pond {
	return Pond{}
}

func (p Pond) sendToPlay(duck Player) {
	duck.play()
}

type Player interface {
	play()
}

type Duck struct {
}

func newDuck() Duck {
	return Duck{}
}

func (d Duck) play() {
	d.float()
	d.cuak()
}

func (d Duck) float() {
	fmt.Println("Duck is floating")
}

func (d Duck) cuak() {
	fmt.Println("Duck is cuaking")
}

type WoodenDuck struct {
}

func newWoodenDuck() WoodenDuck {
	return WoodenDuck{}
}

func (wd WoodenDuck) play() {
	wd.float()
}

func (wd WoodenDuck) float() {
	fmt.Println("WoodenDuck is floating")
}

func main() {
	pond := newPond()
	pond.sendToPlay(newDuck())
	pond.sendToPlay(newWoodenDuck())
}
