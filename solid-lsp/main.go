package main

import "fmt"

//arroja panic cannot use a type B as type A
/* type A struct {
}

func (a A) Test() {
	fmt.Println("Printing A")
}

type B struct {
	A
}

func Imposible(a A) {
	a.Test()
}
func main() {
	a := B{}
	Imposible(a)
} */

type A struct {
}
type tester interface {
	Test()
}

func (a A) Test() {
	fmt.Println("Printing A")
}

type B struct {
	A
}

func Possible(a tester) {
	a.Test()
}
func main() {
	a := B{}
	Possible(a)
}
