package main

import "fmt"

var myBankApi MyBankApi

func init() {
	myBankApi = newMyBankApi()
}

type MyBankApi struct {
}

func newMyBankApi() MyBankApi {
	return MyBankApi{}
}

func (m MyBankApi) charge() {
	fmt.Println("The Bank pays")
}

//---

type MyBankPaymentProcessor struct {
	myBankApi MyBankApi
}

func newMyBankPaymentProcessor(m MyBankApi) MyBankPaymentProcessor {
	return MyBankPaymentProcessor{myBankApi: m}
}

func (m MyBankPaymentProcessor) pay() {
	fmt.Println("Payment Processor Pay's To Bank")
	m.myBankApi.charge()
}

//---

type PaymentProcessor interface {
	pay()
}

//---

type Store struct {
	paymentProcessor PaymentProcessor
}

func newStore(p PaymentProcessor) Store {
	return Store{paymentProcessor: p}
}

func (s Store) purchase() {
	fmt.Println("The store purchases")
	s.paymentProcessor.pay()
}

//---

func main() {
	store := newStore(newMyBankPaymentProcessor(myBankApi))
	store.purchase()
}
